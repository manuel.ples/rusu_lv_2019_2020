fname = input('Unesi ime datoteke: ')  
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

values = []
for line in fhand:
    if 'X-DSPAM-Confidence: ' in line:
        lineInParts = line.split('X-DSPAM-Confidence: ')
        values.append(float(lineInParts[1]))
    
summ = 0.0

for value in values:
    summ += value

print('Average X-DSPAM-Confidence: ', summ / len(values))
