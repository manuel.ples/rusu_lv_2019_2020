fname = input('Unesi ime datoteke: ')  
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

emailAddresses = []
for line in fhand:
    if 'From: ' in line and '@' in line:
        partsOfLine = line.strip()
        partsOfLine = partsOfLine.split('From: ')
        emailAddresses.append(partsOfLine[1])

hostnames = {}
for emailAddress in emailAddresses:   
    emailAdressInParts = emailAddress.split("@")
    hostname = emailAdressInParts[1]
    if hostname not in hostnames:
        hostnames[hostname] = 0
    hostnames[hostname] += 1


print(emailAddresses[:7])
print(hostnames)