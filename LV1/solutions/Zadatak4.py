userInput = ''
solutionArray = []


while userInput != 'Done':
    userInput = input('Unesite željeni broj: ')
    try:
        userInput = float(userInput)
        solutionArray.append(userInput)
    except:
        print ('The value is not a number')
        continue
else:
    print('Broj unešenih elemenata: ', len(solutionArray))
    summ = 0
    for solution in solutionArray:
        summ += solution
    avgValue = summ/len(solutionArray)
    print('Srednja vrijednost elemenata: ', avgValue)
    print('Minimalna vrijednost elemenata: ', min(solutionArray))
    print('Maksimalna vrijednost elemenata: ', max(solutionArray))