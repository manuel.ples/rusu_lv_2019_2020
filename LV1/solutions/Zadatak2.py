ocjena = float(input('Unesite ocjenu od 0.0 i 1.0: '))
if ocjena > 0.0 and ocjena < 1.0:
    if ocjena >= 0.9:
        print ('A')
    elif ocjena >= 0.8:
        print('B')    
    elif ocjena >= 0.7:
        print('C')     
    elif ocjena >= 0.6:
        print('D')       
    elif ocjena < 0.6:
        print('F')    
else :
    print('Unesena vrijednost nije u rasponu od 0.0 do 1.0')        