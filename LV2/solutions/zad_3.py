import numpy as np
import matplotlib.pyplot as plt

def getHeight(person):
    if person == 1:
        return np.random.normal(loc = 180, scale = 7)
    else:
        return np.random.normal(loc = 167, scale = 7)

randomNumbers = np.random.randint(2, size = 1000)

people = np.array(randomNumbers)
print(people)

heights = []
for person in people:
    heights.append(getHeight(person))

heightsVector = np.array(heights)

menHeight = []
womenHeight = []
for i in range(len(heights)):
    if people[i] == 1:
        menHeight.append(heights[i])
    else:
        womenHeight.append(heights[i])

#CRTANJE
menHeightVector = np.array(menHeight)
womenHeightVector = np.array(womenHeight)
#print(menHeight)
#print(womenHeight)
t1 = range(len(menHeightVector))
t2 = range(len(womenHeightVector))
plt.plot(t1, menHeightVector, 'b')
plt.plot(t2, womenHeightVector, 'r')

menHeightAvgValue = menHeightVector.mean()
womenHeightAvgValue = womenHeightVector.mean()
xMax = max(len(menHeightVector), len(womenHeightVector)) - 1
plt.hlines(menHeightAvgValue,0, xMax, 'black')
plt.hlines(womenHeightAvgValue, 0, xMax, 'green')
plt.show()

#print(heightsVector)