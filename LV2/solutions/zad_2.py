import re

fname = input('Unesi ime datoteke: ')  
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

firstParts = []
atLeastOneA = []
oneA = []
noA = []
atLeastOneNumber = []
onlyLowerCase = []

regexStr = r'([^\s<]+)@\S+'
regexAtLeastOneA = re.compile(r"([a-zA-Z0-9\.]*a+[a-zA-Z0-9]*)")
regexOneA = re.compile(r'^([b-zB-Z0-9\.]*a[b-zB-Z0-9]*)$')
regexNoA = re.compile(r'^([b-zB-Z0-9\.]*)$')
regexAtLeastOneNumber = re.compile(r'^([a-zA-Z0-9\.]*[0-9]+[a-zA-Z0-9\.]*)$')
regexOnlyLowerCase = re.compile(r'^([a-z]+)$')

for line in fhand:
    res = re.findall(regexStr, line)
    for i in res:
        firstParts.append(i)

firstParts = list(set(filter(None, firstParts)))
atLeastOneA = list(filter(regexAtLeastOneA.match, firstParts))
oneA = list(filter(regexOneA.match, firstParts))
noA = list(filter(regexNoA.match, firstParts))
atLeastOneNumber = list(filter(regexAtLeastOneNumber.match, firstParts))
onlyLowerCase = list(filter(regexOnlyLowerCase.match, firstParts))



print(firstParts)
print('\n-------------------------------AT LEAST ONE A--------------------------------------')
print(atLeastOneA)
print('\n-------------------------------ONE A--------------------------------------')
print(oneA)
print('\n-------------------------------NO A--------------------------------------')
print(noA)
print('\n-------------------------------AT LEAST ONE NUMBER--------------------------------------')
print(atLeastOneNumber)
print('\n-------------------------------ONLY LOWERCASE LETTERS--------------------------------------')
print(onlyLowerCase)