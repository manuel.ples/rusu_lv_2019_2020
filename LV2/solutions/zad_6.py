import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = mpimg.imread('tiger.png')

data = np.asarray(img)
#data = img
brightnessFactor = 2

resultImg = np.multiply(data, brightnessFactor)
resultImg = np.clip(resultImg, 0, 1)


figure, axes = plt.subplots(nrows=1, ncols=2) #kreiranje figure-a s dva elementa (jedan pored drugog)

axes[0].imshow(img)                 #otvaranje dvije slike na jednom figure-u
axes[1].imshow(resultImg)

plt.show() 
