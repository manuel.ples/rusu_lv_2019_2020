import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

for i in range(1000):
    randomRolls = np.random.randint(1,7, size = 30)

    avgValue, standardDeviation = norm.fit(randomRolls)

    print(avgValue)
    print(standardDeviation)

    plt.plot(avgValue, standardDeviation,'o',color = 'black')
    plt.xlabel("Prosječna vrijednost")
    plt.ylabel("Standardna devijacija")

plt.show()