import re

fname = input('Unesi ime datoteke: ')  
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

firstParts = []
regexStr = r'([^\s<]+)@\S+'
for line in fhand:
     if '@' in line:
        firstParts.append(re.findall(regexStr, line))

print (firstParts)