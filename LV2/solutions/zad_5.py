import numpy as np
import matplotlib.pyplot as plt

def findIndexOf(element, matrix):
    for i, matrix_i in enumerate(matrix):
        for j, value in enumerate(matrix_i):
            if value == element:
                return (i, j)

fname = input('Unesi ime datoteke: ')  
try:
    fhand = open(fname)
except:
    print ('File cannot be opened:', fname)
    exit()

data = []
for line in fhand:
    lineInParts = line.split(',')
    data.append(lineInParts)

dataVector = np.array(data)

mpgIndex = findIndexOf('"mpg"', dataVector)
hpIndex = findIndexOf('"hp"', dataVector)
wtIndex = findIndexOf('"wt"', dataVector)

mpgValues = []
hpValues = []
wtValues = []

for row in dataVector:
    mpgValues.append(row[mpgIndex[1]])
    hpValues.append(row[hpIndex[1]])
    wtValues.append(row[wtIndex[1]])

mpgValues.pop(0)
hpValues.pop(0)
wtValues.pop(0)

mpgValueVector = np.array(mpgValues)
hpValueVector = np.array(hpValues)
wtValueVector = np.array(wtValues)

mpgValueVector = mpgValueVector.astype(np.float)
hpValueVector = hpValueVector.astype(np.float)
wtValueVector = wtValueVector.astype(np.float)

plt.plot(hpValueVector, mpgValueVector,'o', color = 'blue')
plt.xlabel('Konjska snaga')
plt.ylabel('Potrošnja')

print('Minimalna potrošnja: ', min(mpgValueVector))
print('Maksimalna potrošnja: ', max(mpgValueVector))
print('Srednja potrošnja: ', (mpgValueVector.mean()))

plt.show()

