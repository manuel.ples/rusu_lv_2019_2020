import urllib.request
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt

# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=01.1.2017&vrijemeDo=01.12.2017'

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = list(list(root)[i])
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]),utc=True)
    row_s = pd.Series(row)
    row_s.mjerenje = float(row_s.mjerenje)
    row_s.name = i
    df = df.append(row_s)
    i = i + 1

print(df.head(100))


df.vrijeme = pd.to_datetime(df.vrijeme, utc = True)
df.plot(y='mjerenje', x='vrijeme')

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

#DRUGI
sortedData = df.sort_values("mjerenje")
dates = []
for date in sortedData['vrijeme']:
    dates.append(date.to_pydatetime())

datesList = []

for date in dates:
    datesList.append(str(date.day) +'.'+ str(date.month) +'.'+ str(date.year))    
print(datesList[-3:])
print(sortedData['vrijeme'].tail(3))

#TRECI
fig, axes = plt.subplots( nrows=1, ncols=2)

january = df[df.month==1]
missingJanuary = 31 - january.month.count()
february = df[df.month==2]
missingFebruary = 28 - february.month.count()

march = df[df.month==3]
missingMarch = 31 - march.month.count()

april = df[df.month==4]
missingApril = 30 - april.month.count()

may = df[df.month==5]
missingMay = 31 - may.month.count()

june = df[df.month==6]
missingJune = 30 - june.month.count()

july = df[df.month==7]
missingJuly = 31 - july.month.count()

august = df[df.month==8]
missingAugust = 31 - august.month.count()

september = df[df.month==9]
missingSeptember = 30 - september.month.count()

october = df[df.month==10]
missingOctober = 31 - october.month.count()

november = df[df.month==11]
missingNovember = 30 - november.month.count()

december = df[df.month==12]
missingDecember = 31 - december.month.count()

missingValues = {'missingValues': [missingJanuary, missingFebruary, missingMarch, missingApril, missingMay, missingJune, missingJuly, missingAugust, missingSeptember, missingOctober, missingNovember, missingDecember],'month':[1,2,3,4,5,6,7,8,9,10,11,12]}
monthDataFrame = pd.DataFrame(data = missingValues)

monthDataFrame.plot.bar(x='month', y='missingValues', ylabel = 'mpg',title='Nedostajući dani',legend=True)
print(november)
print(december)

#CETVRTI
summer = df[df['month']==6]
winter = df[df['month']==2]

myMonths = df[(df.month == 6)|(df.month == 2)]

myMonths.boxplot('mjerenje','month', ax = axes[0])

#PETI
workingDays=df[df.dayOfweek.isin(range(5))]
weekDays=df[df.dayOfweek.isin(range(5,7))]

workingDays.hist(column = 'mjerenje',ax = axes[1],fc=(0, 0, 1, 0.5),legend = True,color = 'red')
weekDays.hist(column = 'mjerenje',ax = axes[1],fc=(1, 0, 0, 0.5),legend = True, color = 'blue')
axes[1].legend(labels=['Radni dani','Vikend'], fancybox=True, shadow=True)#Maknio sam handles=axes[1].patches jer se boje na legendi ne mjenjau
plt.suptitle("4. i 5. Zadatak")

plt.tight_layout()
plt.show()
