import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')
print(mtcars)
mtcars.sort_values(by = 'mpg',ascending = False, inplace = True)#.car je isto sto i ['car'] jer je dataframe struktura
print('Pet automobila s najvećom potrošnjom su:\n',mtcars.iloc[:5].car.to_string(index = False))#inplace = True radi sortiranje na polju odnosno nema povratne vrijednosti.

#PRVI
eightCylindeCars = mtcars[mtcars.cyl == 8]
print(eightCylindeCars)#.tail(2)-zadnja 2, tail(3) - zadnja 3
#DRUGI
eightCylindeCars.sort_values(['mpg'], ascending = False, inplace = True)
print(eightCylindeCars.head(3))
#TRECI
sixCylinderCars = mtcars[mtcars.cyl == 6]
print(sixCylinderCars['mpg'].mean())
#CETVRTI
fourCylinderCars = mtcars[mtcars.cyl == 4]
print(fourCylinderCars[(mtcars.wt > 2.0) & (mtcars.wt < 2.2)])
#PETI
print(mtcars[mtcars.am == 1])
#SESTI
print('SESTI-----------------\n',mtcars[(mtcars.am == 0)&(mtcars.hp > 100)])
#SEDMI
print(mtcars['wt']*1000)








