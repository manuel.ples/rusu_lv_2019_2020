import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')
fig, axes = plt.subplots( nrows=2, ncols=2)#sharey=True stavi jednake veličine osi svim plotovima u fiure-u

#PRVI
# colors = []
# for cylinder in mtcars.cyl:
#    if cylinder == 4:
#        colors.append('red')
#    if cylinder == 6:
#        colors.append('blue')
#    if cylinder == 8:
#        colors.append('green')
# mtcars.plot.bar(x='cyl', y='mpg', rot=0, color = colors, ax=axes[0][0], ylabel = 'mpg',title='Potrošnja po cilindru',legend=True)
# axes[0][0].legend(labels=['4','6','8'], handles=axes[0][0].patches, fancybox=True, shadow=True)

# print(list(mtcars.groupby('cyl').mpg))
#ILI OVAKO-CTRL+K zatim C je kimentiranje, isto samo sa u je odkomentiranje

groupedCars = mtcars.groupby('cyl').mpg.mean()

colors = []
for cylinder in mtcars.cyl:
    if cylinder == 4:
        colors.append('red')
    if cylinder == 6:
        colors.append('blue')
    if cylinder == 8:
       colors.append('green')

groupedCars.plot.bar(x='cyl', y='mpg', rot=0, ax=axes[0][0], color = ['red','blue','green'], ylabel = 'mpg',title='Potrošnja po cilindru',legend=True)
axes[0][0].legend(labels=['4','6','8'], handles=axes[0][0].patches, fancybox=True, shadow=True)

#DRUGI
mtcars.boxplot('wt','cyl', ax = axes[0][1],figsize = (6,8), layout = (4,1))
plt.suptitle("2. Zadatak")#postavljanje defaultnog title-a(razlicit od ovog title-a ispod)

#TRECI
amCars = mtcars.groupby('am').mpg.mean()
amCars.plot.bar(x='am', y='mpg', rot=0, ax=axes[1][0], color = ['red','blue'], ylabel = 'mpg',title='Potrošnja',legend=True)
axes[1][0].legend(labels=['Automatic','Manual'], handles=axes[1][0].patches, fancybox=True, shadow=True)

#CETVRTI
automaticCars = mtcars[mtcars.am == 0].mean()
manualCars = mtcars[mtcars.am == 1].mean()

automaticCarsData = {'qsec':[automaticCars.qsec, manualCars.qsec],'hp':[automaticCars.hp, manualCars.hp]}
automaticCarsdataFrame = pd.DataFrame(data = automaticCarsData)

automaticCarsdataFrame.plot.bar(x='qsec', y='hp', rot=0, ax=axes[1][1], color = ['red','blue'], ylabel = 'Snaga',title='Omjer ubrzanja i snage',legend=True)
axes[1][1].legend(labels=['Automatic','Manual'], handles=axes[1][1].patches, fancybox=True, shadow=True)

plt.tight_layout()
plt.show() 